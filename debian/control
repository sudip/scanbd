Source: scanbd
Section: misc
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 debhelper (>= 11),
 libconfuse-dev,
 libdbus-1-dev,
 libsane-dev,
 libudev-dev,
 libusb-1.0-0-dev | libusb-dev,
 pkg-config,
Standards-Version: 4.1.4
Homepage: http://scanbd.sf.net/
Vcs-Browser: https://salsa.debian.org/debian/scanbd
Vcs-Git: https://salsa.debian.org/debian/scanbd.git

Package: scanbd
Architecture: any
Depends:
 lsb-base (>= 3.0-6),
 openbsd-inetd | inet-superserver,
 sane-utils,
 update-inetd,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Scanner button daemon
 It polls the scanner buttons looking for buttons pressed or function knob
 changes or other scanner events as paper inserts / removals and at the same
 time allows also scan-applications to access the scanners. If buttons are
 pressed, etc., various actions can be submitted (scan, copy, email, ...) via
 action scripts. The function knob values are passed to the action-scripts as
 well.
 .
 Scan actions are also signaled via D-Bus. This can be useful for foreign
 applications. Scans can also be triggered via D-Bus from foreign applications.
 .
 On platforms which support signaling of dynamic device insertion / removal
 (libudev, D-Bus, hal) scanbd supports this as well. scanbd can use all
 sane-backends or some special backends from the (old) scanbuttond project.
 .
 This package is a successor of scanbuttond.
